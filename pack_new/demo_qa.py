from selenium import webdriver
from selenium.webdriver.common.by import By
from selenium.webdriver.support.ui import WebDriverWait
from selenium.webdriver.support import expected_conditions as EC
from webdriver_manager.chrome import ChromeDriverManager
import time

first_name = 'Nik'
last_name = 'Kud'
email = 'NikKud@mail.ru'
mobile_number = '7987182059'
gender_man = 'Male'

try:
    browser = webdriver.Chrome(ChromeDriverManager().install())
    browser.implicitly_wait(3)
    browser.get('chrome://settings/')
    browser.execute_script('chrome.settingsPrivate.setDefaultZoom(0.4);')
    browser.get('https://demoqa.com/automation-practice-form')
    browser.find_element(By.XPATH, "//input[@id='firstName']").send_keys(first_name)
    browser.find_element(By.XPATH, "//input[@id='lastName']").send_keys(last_name)
    browser.find_element(By.XPATH, "//input[@id='userEmail']").send_keys(email)
    browser.find_element(By.XPATH, f"//label[text()='{gender_man}']").click()
    browser.find_element(By.XPATH, "//input[@id='userNumber']").send_keys(mobile_number)
    WebDriverWait(browser,5).until(
        EC.element_to_be_clickable((By.XPATH, "//button[@id='submit']"))
    ).click()
    student_name = browser.find_element(By.XPATH, "//td[text()='Student Name']/../td[text()='Nik Kud']").text
    assert f'{first_name} {last_name}' == student_name, f'Требуемое значение {first_name} {last_name} не равно ожидаемому {student_name}'
    student_email = browser.find_element(By.XPATH, "//td[text()='Student Email']/../td[text()='NikKud@mail.ru']").text
    assert email == student_email, f'Требуемое значение {email} не равно ожидаемому {student_email}'
    gender = browser.find_element(By.XPATH, "//td[text()='Gender']/../td[text()='Male']").text
    assert gender_man == gender, f'Требуемое значение {gender_man} не равно ожидаемому {gender}'
    mobile = browser.find_element(By.XPATH, "//td[text()='Mobile']/../td[text()='7987182059']").text
    assert mobile_number == mobile, f'Требуемое значение {mobile_number} не равно ожидаемому {mobile}'
finally:
    time.sleep(3)
    time.sleep(3)
    time.sleep(15)
    browser.quit()




